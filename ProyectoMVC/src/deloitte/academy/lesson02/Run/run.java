package deloitte.academy.lesson02.Run;

import java.util.ArrayList;
import java.util.List;

import deloitte.academy.lesson02.Controller.ControllerAnimales;
import deloitte.academy.lesson02.Model.Animales;

/**
 * Clase run que permite ejecutar el proyecto. 
 * @author jenrubio
 *
 */
public class run {
	public static final List<Animales> listaAnimales = new ArrayList<Animales>();

	public static void main(String[] args) {
		Animales animalImp = new Animales();
		Animales animal1 = new Animales("Le�n", "Panthera leo", "20/10/2000", 19, "Femenino", 
				1.57, 7, 8, 9, 10, 
				11, 12, 13, 14, 15, 
				16, 17, 18, 19, 20);
		ControllerAnimales cA = new ControllerAnimales();
		cA.Agregar(animal1);
		
		System.out.println("Buscar por nombre: ");
		animalImp = cA.buscarPorNombre("Le�n");
		System.out.println("Nombre com�n: " + animalImp.getNombreComun() + "\nCient�fico: " + animalImp.getNombreCientifico() + 
				"\nFecha de nacimiento: " + animalImp.getFechaNacimiento() + "\nEdad: " + animalImp.getEdad() + 
				"\nSexo: " + animalImp.getSexo() + "\nLongitud: "+ animalImp.getLongitud() + "\nArg7: " + animalImp.getArg17());
		
		 
		System.out.println("Buscar todos");
		cA.buscarTodos();
	}
}
