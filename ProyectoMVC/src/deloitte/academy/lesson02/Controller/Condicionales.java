package deloitte.academy.lesson02.Controller;

/**
 * Clase que contiene 5 m�todos condicionales.
 * 
 * @author jenrubio (jenrubio@externosdeloittemx.com)
 * @since 2020-03-11
 * @version 1.0
 */
public class Condicionales {

	/**
	 * M�todo con operador ternario que permite determinar cual de los dos n�meros
	 * es el mayor.
	 * 
	 * @param num1: n�mero de tipo entero.
	 * @param num2: n�mero de tipo entero.
	 * @return num1 o num2 dependiendo de cu�l sea el mayor.
	 */
	public int numeroMayor(int num1, int num2) {
		return (num1 > num2) ? num1 : num2;
	}

}
