package deloitte.academy.lesson02.Controller;

import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import deloitte.academy.lesson02.Model.Animales;
import deloitte.academy.lesson02.Run.run;

public class ControllerAnimales {
	private static final Logger LOGGER = Logger.getLogger(Logger.class.getName());

	Animales animalPrueba = new Animales();

	/**
	 * M�todo que permite agregar un objeto Animal a una lista.
	 * 
	 * @param animal: Objeto de la clase Animal.
	 * @exception ex: Mensaje que indica que ocurri� un error.
	 */
	public void Agregar(Animales animal) {
		try {
			run.listaAnimales.add(animal);
			LOGGER.info("Animal agregado correctamente.");
		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Ocurri� un error, vuelve a intentarlo", ex);
		}
	}

	/**
	 * M�todo que recibe cada uno de los par�metros de los objetos y permite 
	 * que estos posteriormente se agreguen a la lista de Animales. 
	 * @param nombreComun: (String) Nombre por el que es conocido el animal. 
	 * @param nombreCientifico: (String) Nombre cient�fico del animal. 
	 * @param fechaNacimiento: (String) fecha con formato dd/mm/yyyy
	 * @param edad: (int) edad del animal. Si tiene meses colocar 0.
	 * @param sexo: (String) Femenino o Masculino. 
	 * @param longitud: (Double) longitud en metros.
	 * @param arg7:  (int) dato sin definir.
	 * @param arg8:  (int) dato sin definir.
	 * @param arg9:  (int) dato sin definir.
	 * @param arg10: (int) dato sin definir.
	 * @param arg11: (int) dato sin definir.
	 * @param arg12: (int) dato sin definir.
	 * @param arg13: (int) dato sin definir.
	 * @param arg14: (int) dato sin definir.
	 * @param arg15: (int) dato sin definir.
	 * @param arg16: (int) dato sin definir.
	 * @param arg17: (int) dato sin definir.
	 * @param arg18: (int) dato sin definir.
	 * @param arg19: (int) dato sin definir.
	 * @param arg20: (int) dato sin definir.
	 * @exception ex: Indica si hubo un problema. 
	 */
	public void agregarPorAtributos(String nombreComun, String nombreCientifico, String fechaNacimiento, int edad, String sexo, 
			double longitud, int arg7, int arg8, int arg9, int arg10, 
			int arg11, int arg12, int arg13, int arg14, int arg15, 
			int arg16, int arg17, int arg18, int arg19, int arg20) {
		try {
			/** Se van a�adiendo uno a uno los atributos */
			animalPrueba.setNombreComun(nombreComun);
			animalPrueba.setNombreCientifico(nombreCientifico);
			animalPrueba.setFechaNacimiento(fechaNacimiento);
			animalPrueba.setEdad(edad);
			animalPrueba.setSexo(sexo);
			animalPrueba.setLongitud(longitud);
			animalPrueba.setArg7(arg7);
			animalPrueba.setArg8(arg8);
			animalPrueba.setArg9(arg9);
			animalPrueba.setArg10(arg10);
			animalPrueba.setArg11(arg11);
			animalPrueba.setArg12(arg12);
			animalPrueba.setArg13(arg13);
			animalPrueba.setArg14(arg14);
			animalPrueba.setArg15(arg15);
			animalPrueba.setArg16(arg16);
			animalPrueba.setArg17(arg17);
			animalPrueba.setArg18(arg18);
			animalPrueba.setArg19(arg19);
			animalPrueba.setArg20(arg20);
			run.listaAnimales.add(animalPrueba); /** Este objeto tipo Animal se a�ade a la lista */

			LOGGER.info("Animal agregado correctamente.");

		} catch (Exception ex) {
			LOGGER.log(Level.SEVERE, "Ocurri� un error, vuelve a intentarlo", ex);
		}
	}
	
	/**
	 * M�todo que permite buscar a un animal en la lista de animales 
	 * mediante el nombre com�n.Se utiliza la expresi�n lambda para recorrer la lista. 
	 * @param nombreComun: Se tiene que ingresar el nombre com�n mediante 
	 * un dato del tipo string. 
	 * @return animalPrueba: regresa el objeto animalPrueba si es que se encontr�
	 * ese nombre en la lista.
	 */
	public Animales buscarPorNombre(String nombreComun) {
		run.listaAnimales.forEach(new Consumer<Animales>(){

			@Override
			public void accept(Animales animal) {
				if(animal.getNombreComun() == nombreComun) {
					animalPrueba = animal;
				}
			}
		});
		return animalPrueba;
	}
	
	/**
	 * M�todo que no recibe par�metos y no regresa nada. 
	 * �nicamente imprime el nombre com�n y cient�fico del animal. 
	 */
	public void buscarTodos(){
		run.listaAnimales.forEach(new Consumer<Animales>() {

			@Override
			public void accept(Animales animal) {
				System.out.println("Nombre com�n: " + animal.getNombreComun() 
				+ "\nNombre cient�fico: " + animal.getNombreCientifico());
				
			}
			
		});
	}

}
