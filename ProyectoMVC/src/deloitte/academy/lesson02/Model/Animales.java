package deloitte.academy.lesson02.Model;

/**
 * Clase Animales del tipo entity. Contiene 20 atributos, de los cuales 6 ya han
 * sido definidos.
 * @author jenrubio
 * @since 2020-03-11
 */
public class Animales {
	/** Declaración de variables */
	private String nombreComun;
	private String nombreCientifico;
	private String fechaNacimiento;
	private int edad;
	private String sexo;
	private double longitud;
	private int arg7;
	private int arg8;
	private int arg9;
	private int arg10;
	private int arg11;
	private int arg12;
	private int arg13;
	private int arg14;
	private int arg15;
	private int arg16;
	private int arg17;
	private int arg18;
	private int arg19;
	private int arg20;

	/** Constructores */

	public Animales() {
		// TODO Auto-generated constructor stub
	}


	public Animales(String nombreComun, String nombreCientifico, String fechaNacimiento, int edad, String sexo,
			double longitud, int arg7, int arg8, int arg9, int arg10, 
			int arg11, int arg12, int arg13, int arg14, int arg15, 
			int arg16, int arg17, int arg18, int arg19, int arg20) {
		super();
		this.nombreComun = nombreComun;
		this.nombreCientifico = nombreCientifico;
		this.fechaNacimiento = fechaNacimiento;
		this.edad = edad;
		this.sexo = sexo;
		this.longitud = longitud;
		this.arg7 = arg7;
		this.arg8 = arg8;
		this.arg9 = arg9;
		this.arg10 = arg10;
		this.arg11 = arg11;
		this.arg12 = arg12;
		this.arg13 = arg13;
		this.arg14 = arg14;
		this.arg15 = arg15;
		this.arg16 = arg16;
		this.arg17 = arg17;
		this.arg18 = arg18;
		this.arg19 = arg19;
		this.arg20 = arg20;
	}

	/** Getters y setters */
	
	public String getNombreComun() {
		return nombreComun;
	}

	public void setNombreComun(String nombreComun) {
		this.nombreComun = nombreComun;
	}

	public String getNombreCientifico() {
		return nombreCientifico;
	}

	public void setNombreCientifico(String nombreCientifico) {
		this.nombreCientifico = nombreCientifico;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public double getLongitud() {
		return longitud;
	}

	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}

	public int getArg7() {
		return arg7;
	}

	public void setArg7(int arg7) {
		this.arg7 = arg7;
	}

	public int getArg8() {
		return arg8;
	}

	public void setArg8(int arg8) {
		this.arg8 = arg8;
	}

	public int getArg9() {
		return arg9;
	}

	public void setArg9(int arg9) {
		this.arg9 = arg9;
	}

	public int getArg10() {
		return arg10;
	}

	public void setArg10(int arg10) {
		this.arg10 = arg10;
	}

	public int getArg11() {
		return arg11;
	}

	public void setArg11(int arg11) {
		this.arg11 = arg11;
	}

	public int getArg12() {
		return arg12;
	}

	public void setArg12(int arg12) {
		this.arg12 = arg12;
	}

	public int getArg13() {
		return arg13;
	}

	public void setArg13(int arg13) {
		this.arg13 = arg13;
	}

	public int getArg14() {
		return arg14;
	}

	public void setArg14(int arg14) {
		this.arg14 = arg14;
	}

	public int getArg15() {
		return arg15;
	}

	public void setArg15(int arg15) {
		this.arg15 = arg15;
	}

	public int getArg16() {
		return arg16;
	}

	public void setArg16(int arg16) {
		this.arg16 = arg16;
	}

	public int getArg17() {
		return arg17;
	}

	public void setArg17(int arg17) {
		this.arg17 = arg17;
	}

	public int getArg18() {
		return arg18;
	}

	public void setArg18(int arg18) {
		this.arg18 = arg18;
	}

	public int getArg19() {
		return arg19;
	}

	public void setArg19(int arg19) {
		this.arg19 = arg19;
	}

	public int getArg20() {
		return arg20;
	}

	public void setArg20(int arg20) {
		this.arg20 = arg20;
	}
	

}
